from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin
from .models import CustomUser

admin.site.site_header = "Chalchitra Admin"
admin.site.site_title = "Chalchitra Admin Dashboard"
admin.site.index_title = "Welcome to Chalchitra Admin Dashboard"

class CustomUserAdmin(ModelAdmin):
    list_display = ['email', 'password', 'phone_number', 'first_name', 'last_name','date_joined', 'is_staff', 'is_active']
    search_fields = ['email', 'phone_number', 'first_name', 'last_name']


admin.site.register(CustomUser, CustomUserAdmin)