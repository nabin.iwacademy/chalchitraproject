from __future__ import unicode_literals

from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    phone_number = models.CharField(max_length=16)
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)





#
# from django.db import models
# from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
#
#
# class MyAccountManager(BaseUserManager):
#     def create_user(self, email, phone_number, password=None):
#         if not email:
#             raise ValueError("User must have an email address")
#         if not phone_number:
#             raise ValueError("User must have a phone number")
#
#         user = self.model(
#             email=self.normalize_email(email),
#             phone_number=phone_number,
#         )
#         user.set_password(password)
#         user.save(user=self._db)
#         return user
#
#     def create_superuser(self, email, phone_number, password):
#         user = self.model(
#             email=self.normalize_email(email),
#             phone_number=phone_number,
#             password=password
#
#         )
#         user.is_admin = True
#         user.is_staff = True
#         user.is_superuser = True
#         user.save(using=self._db)
#         return user
#
#
# class CustomUser(AbstractUser, PermissionsMixin):
#     email = models.EmailField('email address', unique=True, max_length=100)
#     first_name = models.CharField('First Name', max_length=255, blank=True, null=False)
#     last_name = models.CharField('Last Name', max_length=255, blank=True, null=False)
#     date_joined = models.DateTimeField('date joined', auto_now_add=True)
#     phone_number = models.CharField(max_length=15)
#     is_active = models.BooleanField(default=True)
#     is_staff = models.BooleanField(default=False)
#     is_superuser = models.BooleanField(default=False)
#     is_admin = models.BooleanField(default=False)
#
#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = ['phone_number']
#
#     objects = MyAccountManager()
#
#     def __str__(self):
#         return self.email
#
#     def has_perm(self, perm, obj=None):
#         return self.is_admin
#
#     def has_module_perms(self, app_label):
#         return True
