from rest_framework.routers import DefaultRouter
from Chalchitra.accounts.api.v1.views import AuthViewSet
from Chalchitra.movie.api.v1.views.MovieShowDetailView import MovieShowDetails
r = DefaultRouter(trailing_slash=False)
r.register('api/auth', AuthViewSet, basename='auth')

urlpatterns = [

]+r.urls