from django.db import models

# Create your models here.

class MovieThreater(models.Model):
    threater_name = models.CharField(max_length=20)
    threater_type = models.CharField(max_length=10)
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=40)
    exact_location = models.CharField(max_length=50)

    def __str__(self):
        return self.threater_name


class Auditoriums(models.Model):
    threater_id = models.ForeignKey(MovieThreater, on_delete=models.CASCADE)
    auditorium_identifier = models.CharField(max_length=30)
    seat_capacity = models.IntegerField()
    seat_available = models.IntegerField()


    def __str__(self):
        return self.auditorium_identifier

