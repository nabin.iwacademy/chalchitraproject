from rest_framework.serializers import ModelSerializer
from Chalchitra.threater.models import Auditoriums


class AuditoriumDetailsSerializer(ModelSerializer):
    class Meta:
        model = Auditoriums
        fields = '__all__'
