from rest_framework.viewsets import ModelViewSet
from Chalchitra.threater.models import Auditoriums
from Chalchitra.threater.api.v1.serializer.AudotiriumDetailsSerializer import AuditoriumDetailsSerializer


class AufitorumDetailsView(ModelViewSet):
    serializer_class = AuditoriumDetailsSerializer
    queryset = Auditoriums.objects.all()