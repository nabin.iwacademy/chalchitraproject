from django.db import models
from Chalchitra.threater.models import Auditoriums


class Movie(models.Model):
    CHOICES = (
        ('en', 'English'),
        ('np', 'Nepali'),
        ('hi', 'Hindi'),
    )

    movie_name = models.CharField(max_length=100)
    movie_cover_image = models.ImageField(upload_to='media/')
    movie_description = models.TextField()
    movie_trailor_url = models.URLField()
    movie_language = models.CharField(choices=CHOICES, max_length=4)
    release_date = models.DateTimeField(null=True)
    movie_duration = models.TextField(default=120)

    def __str__(self):
        return self.movie_name


class MovieShowTime(models.Model):
    movie_id = models.ForeignKey(Movie, on_delete=models.CASCADE)
    audotirium_id = models.ForeignKey(Auditoriums, on_delete=models.CASCADE)
    start_date_time = models.DateTimeField()


