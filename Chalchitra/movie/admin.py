from django.contrib import admin
from .models import Movie, MovieShowTime
# Register your models here.



admin.site.register(Movie)
admin.site.register(MovieShowTime)