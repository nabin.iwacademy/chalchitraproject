from rest_framework.serializers import ModelSerializer
from Chalchitra.movie.models import MovieShowTime
from .MovieDetailsSerializer import MovieDetailsSerializer
from Chalchitra.threater.api.v1.serializer.AudotiriumDetailsSerializer import AuditoriumDetailsSerializer


class MovieShowDetailSerializer(ModelSerializer):
    class Meta:
        model = MovieShowTime
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        s = MovieDetailsSerializer(instance.movie_id)
        response['movie_id'] = s.data
        s = AuditoriumDetailsSerializer(instance.audotirium_id)
        response['audotirium_id'] = s.data
        return response
