from rest_framework.serializers import ModelSerializer
from Chalchitra.movie.models import Movie


class MovieDetailsSerializer(ModelSerializer):

    class Meta:
        model = Movie
        fields = '__all__'


