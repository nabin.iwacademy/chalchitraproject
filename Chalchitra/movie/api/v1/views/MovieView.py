from rest_framework.viewsets import ModelViewSet
from Chalchitra.movie.models import Movie
from Chalchitra.movie.api.v1.serializer.MovieDetailsSerializer import MovieDetailsSerializer


class MovieDetailView(ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieDetailsSerializer