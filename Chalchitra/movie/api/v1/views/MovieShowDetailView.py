from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from Chalchitra.movie.models import Movie, MovieShowTime
from Chalchitra.movie.api.v1.serializer.MovieShowDetailSerializer import MovieShowDetailSerializer


class MovieShowDetails(ModelViewSet):
    queryset = MovieShowTime.objects.all()
    serializer_class = MovieShowDetailSerializer

    def list(self, request, *args, **kwargs):
        request_get = self.get_serializer(self.get_queryset(), many=True)
        r ={
            "version": "1.0.0",
            "statusCode": 200,
            "result":request_get.data
        }
        return Response(r)
