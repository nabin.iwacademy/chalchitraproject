from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from Chalchitra.movie.api.v1.views.MovieView import MovieDetailView
from Chalchitra.movie.api.v1.views.MovieShowDetailView import MovieShowDetails
r = DefaultRouter()
r.register('api/movie', MovieDetailView, basename='movie')
r.register('api/showtime', MovieShowDetails, basename='movieshows')
urlpatterns = [

]+r.urls